<?php 

class Customer {

    private $id;
    private $name;

    function __construct() {
    }

    public function addName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function addId($id) {
        $this->id = $id;
    }
    public function getId() {
        return $this->id;
    }
}
