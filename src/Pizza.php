<?php
class Pizza {

    private $id;
    private $topping = array();
 
    function __construct () {
    }

    public function addId($id) {
        $this->id = (int) $id;
    }

    public function getId() {
        return (int) $this->id;
    }

    public function addTopping($topping) {
        array_push($this->topping, $topping);
    }

    public function getTopping() {
        return array_shift($this->topping);
    }
}
