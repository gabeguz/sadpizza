<?php
class PizzaOrderFromWeb {

    private $cleanRequest;
    private $dbh;
    private $ds;
    private $order;

    function __construct($request) {
        $this->sanitizeRequest($request);
        $this->dbh = new PDO('mysql:host=localhost;dbname=sadpizza', 'sadpizza', 'SzvaqgujIT5zKea6xzYqsx0bQa31ikNfHv0WvOK');
        $this->ds = new MysqlPizzaOrderDataStore($this->dbh);
        $this->createPizzaOrder();
    }

    private function sanitizeRequest($request) {
        $this->cleanRequest['name'] = filter_var($request['name'], FILTER_SANITIZE_STRING);
        $this->cleanRequest['topping_1'] = filter_var($request['topping_1'], FILTER_SANITIZE_STRING);
        $this->cleanRequest['topping_2'] = filter_var($request['topping_2'], FILTER_SANITIZE_STRING);
        $this->cleanRequest['topping_3'] = filter_var($request['topping_3'], FILTER_SANITIZE_STRING);
    }

    public function getOrder() {
        return $this->order;
    }

    private function createPizzaOrder() {
        $customer = new Customer();
        $customer->addName($this->cleanRequest['name']);
        $pizza = new Pizza();
        $pizza->addTopping($this->cleanRequest['topping_1']);
        $pizza->addTopping($this->cleanRequest['topping_2']);
        $pizza->addTopping($this->cleanRequest['topping_3']);

        $order = new PizzaOrder($customer, $pizza);
        $id = $order->place($this->ds);
        $this->order = $order;
    }
}
