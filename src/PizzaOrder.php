<?php

class PizzaOrder implements Orderable {

    private $id;
    private $pizza;
    private $customer;
    private $status;

    function __construct(Customer $customer, Pizza $pizza) {
        $this->customer = $customer;
        $this->pizza = $pizza;
    }

    public function getCustomer() {
        return $this->customer;
    }

    public function getPizza() {
        return $this->pizza;
    }

    public function getStatus() {
        return $this->status;
    }

    public function addStatus($status) {
        $this->status = $status;
    }

    public function place($dataStore) {
        $this->status = 'todo';
        $this->id = $dataStore->create($this);
        return $this->id;
    }
}
