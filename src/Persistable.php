<?php
interface Persistable {
    public function create($dto);
    public function read($dto);
    public function update($dto);
    public function delete($dto);
}
