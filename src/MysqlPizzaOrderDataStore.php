<?php
// This is a mysql persistable data store.  It persists PizzaOrder 
//  Data Transfer Objects to mysql tables.

class MysqlPizzaOrderDataStore implements Persistable {

    private $dbh;

    const createCustomerStatement = "INSERT INTO customer (name) VALUES (:name)";
    const createOrderStatement = "INSERT INTO pizza (customer_id, topping_1, topping_2, topping_3, status) VALUES (:customer_id, :topping_1, :topping_2, :topping_3, :status)";
    const readMultipleTodoOrdersStatement = "SELECT p.id as id, c.id as customer_id, c.name as name, p.topping_1 as topping_1, p.topping_2 as topping_2, p.topping_3 as topping_3, p.status as status FROM pizza p LEFT JOIN customer c ON p.customer_id = c.id WHERE p.status = 'todo'";

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    public function create($order) {
        $customer = $order->getCustomer();
        $stmt = $this->dbh->prepare($this::createCustomerStatement);
        $stmt->bindParam(':name', $customer->getName());
        $this->dbh->beginTransaction();
        $stmt->execute();
        $customer->addId($this->dbh->lastInsertId());
        $pizza = $order->getPizza();
        $stmt = $this->dbh->prepare($this::createOrderStatement);
        $stmt->bindParam(':customer_id', $customer->getId());
        $stmt->bindParam(':topping_1', $pizza->getTopping());
        $stmt->bindParam(':topping_2', $pizza->getTopping());
        $stmt->bindParam(':topping_3', $pizza->getTopping());
        $stmt->bindParam(':status', $order->getStatus());
        $stmt->execute();
        $pizza->addId($this->dbh->lastInsertId());
        $this->dbh->commit();
        return $pizza->getId();
    }

    public function read($order) {
    }

    public function update($order) {
    }

    public function delete($order) {
    }

    public function readMultipleTodoOrders() {
        $stmt = $this->dbh->prepare($this::readMultipleTodoOrdersStatement);
        $stmt->execute();
        $orders = array();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $customer = new Customer();
            $customer->addId($row['customer_id']);
            $customer->addName($row['name']);
            $pizza = new Pizza();
            $pizza->addId($row['id']);
            $pizza->addTopping($row['topping_1']);
            $pizza->addTopping($row['topping_2']);
            $pizza->addTopping($row['topping_3']);
            $order = new PizzaOrder($customer, $pizza);
            $order->addStatus($row['status']);
            $orders[] = $order;
        }
        return $orders;
    }
}
