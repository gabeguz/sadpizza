<?php
interface Orderable {

    public function place($dataStore);
}
