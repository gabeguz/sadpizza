<?php
// This is a test persistable data store, it doesn't actually persist
//  It only returns expected results for unit tests.

class MemoryPizzaOrderDataStore implements Persistable {
    public function create($order) {
        return 23;
    }

    public function read($order) {
    }

    public function update($order) {
    }

    public function delete($order) {
    }
}
