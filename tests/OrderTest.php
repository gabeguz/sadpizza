<?php 

class OrderTest extends PHPUnit_Framework_TestCase {

    public function testCanBeCreated() {
        $customer = new Customer();
        $pizza = new Pizza();
        $order = new PizzaOrder($customer, $pizza);
        $this->assertInstanceOf('PizzaOrder', $order, 'order is not an PizzaOrder!');
    }

    public function testStatusOfANewOrderIsNULL() {
        $customer = new Customer();
        $pizza = new Pizza();
        $order = new PizzaOrder($customer, $pizza);
        $this->assertNull($order->getStatus());
    }

    public function testOrderHasPizzaAndCustomer() {
        $customer = new Customer();
        $pizza = new Pizza();
        $order = new PizzaOrder($customer, $pizza);
        $this->assertInstanceOf('Customer', $order->getCustomer());
        $this->assertInstanceOf('Pizza', $order->getPizza());
    }

    public function testOrderCanBeCompleted() {
        $customer = new Customer();
        $pizza = new Pizza();
        $order = new PizzaOrder($customer, $pizza);
        $ds = new MemoryPizzaOrderDataStore();
        $id = $order->place($ds);
        $this->assertInternalType('integer', $id);
        $this->assertEquals('todo', $order->getStatus());
    }

    /**
     * @group db
     */
    public function testOrderCanBeStoredInMysql() {
        $dbh = new PDO('mysql:host=localhost;dbname=sadpizza', 'sadpizza', 'SzvaqgujIT5zKea6xzYqsx0bQa31ikNfHv0WvOK');
        $ds = new MysqlPizzaOrderDataStore($dbh);
        $customer = new Customer();
        $customer->addName("Gabriel Guzman");
        $pizza = new Pizza();
        $pizza->addTopping("Pepperoni");
        $order = new PizzaOrder($customer, $pizza);
        $id = $order->place($ds);
        $this->assertInternalType('integer', $id);
        $this->assertEquals('todo', $order->getStatus());
    }

    /**
     * @group db
     */
    public function testTodoOrdersCanBeFetchedFromMysql() {
        $dbh = new PDO('mysql:host=localhost;dbname=sadpizza', 'sadpizza', 'SzvaqgujIT5zKea6xzYqsx0bQa31ikNfHv0WvOK');
        $ds = new MysqlPizzaOrderDataStore($dbh);
        $orders = $ds->readMultipleTodoOrders();
        $this->assertInstanceOf('PizzaOrder', $orders[0]);
        $this->assertInstanceOf('Pizza', $orders[0]->getPizza());
        $this->assertInstanceOf('Customer', $orders[0]->getCustomer());
    }
}
