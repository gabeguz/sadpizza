<?php

class PizzaTest extends PHPUnit_Framework_TestCase {

    public function testCanBeCreated() {
        $pizza = new Pizza();
        $this->assertInstanceOf('Pizza', $pizza, "pizza is not a Pizza!");
    }

    public function testCanAddTopping() {
        $pizza = new Pizza();
        $pizza->addTopping('pepperoni');
    }

    public function testCanGetTopping() {
        $pizza = new Pizza();
        $pizza->addTopping('pepperoni');
        $this->assertEquals('pepperoni', $pizza->getTopping());
    }

    public function testCanAddUpToThreeToppings() {
        $pizza = new Pizza();
        $pizza->addTopping('pepperoni');
        $pizza->addTopping('mushrooms');
        $pizza->addTopping('onions');
        $this->assertEquals('pepperoni', $pizza->getTopping());
        $this->assertEquals('mushrooms', $pizza->getTopping());
        $this->assertEquals('onions', $pizza->getTopping());
    }
}
