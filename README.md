Here be the SadPizza application.  

Introduction
============

I attempted to keep things as simple as possible while still delivering value for the stakeholders (you).  I took the initial description of the problem, and converted it to user-stories that had clear acceptance criteria.  Like this you know what I was shooting for, and you know if there was any mis-communication.  The 4 stories I created were: 

1. As Joe the surfer I would like to see a list of available pizzas so I can choose the one I want to order
** Verify that I can browse to www.sadpizza.com and see the list of available pizza choices

2. As Joe the surfer I would like to choose an available pizza so I can place an order
** Verify I can select a pizza from the list of pizzas available

3. As Joe the surfer I would like to place an order online so I don't have to call SadPizza
** Verify I can submit my order once I have made a selection
** Verify I recieve a confirmation that my order has been recieved
** Verify that I can leave my name when placing an order

4. As Chris the pizza place owner, I would like to see pending orders so I know who wants what.
** Verify I can see a list of pending orders
** Verify I can mark an order as completed


Stories one through 3 were completed and delivered.  Story 4 will have to be pushed to another sprint, as I was unable to complete it on time.   Though one could argue that phpmyadmin is a suitable interface for the pizza place owner, and does meet the criteria, I won't argue that here. 

Number four wasn't explicitly mentioned in the task you sent me, but I figured I should add it for compelteness sake.  

Assumptions
===========
I made the assumption that the "app" you were asking for was a web application.  Though my code could be easily adapted to the command line, what I focused on was the web.  

Frameworks
==========

phpunit : https://phpunit.de/
I built this application using TDD - phpunit was the framework I used for running my tests, and confirming that my build was good.

bootstrap : http://getbootstrap.com/
I used bootstrap as my front-end framework since I knew with that I'd be able to get a prototype up and running very quickly, and that the design would be responsive and scale well from a phone -> tablet -> desktop. 

Backend Frameworks
==================

I chose not to use any backend frameworks for this project.  Though Zend framework, or Symphony could have been useful, as this project was small, I simply hand coded the classes that I needed.  While not sticking to a framework, I did stick to several best practices and design patterns: 

di - Dependancy Injection -> My persistance classes have their dependancies injected into them, so they can easily be swapped out.  This is especially useful while doing TDD, as it allows you to easily mock a result for testing. It's also handy if you want to try out a new datastore (redis, postgres, couchdb) as you only have to create a new xxxPizzaOrderDataStore.php class, and inject that into your PizzaOrder at place() time to try it out.  Your business logic will continue as usual.

dao/dto - Data Access Objects and Data Transfer Objects are perhaps my favorite patters for persisting data to a DB.  Slightly different from an ORM, DAO/DTO does not enforce a mapping between objects and tables.  As you can see in my code, there is one DAO for the multiple DTO's.  This allows me to centralize all my sql in a single audit point, while still allowing the user of my classes to not have to care about how an object is persisted.

mvc (ish) - While there could be some discussion about how *strictly* I applied MVC - I believe that the general criteria are met.  I have seperated presentaion logic from business logic.  There is definitely danger of abuse in my setup, as the concerns are not rigidly seperated, that is -- if a developer wanted to, he could easily ask for things that would violate MVC. 

Next Steps
==========
If this were an actual sprint, the places we'd go from here would be to tighten up error handling/notifications.  Currently, there is no input validation on the client side, and server side validation is transparent -- that is, the user is not notified if the server didn't like the users data - the server just fixes is and pushes on.  I focused instead on preventing SQL injection and XSS attacks -- all user input is sanitized using php's filter_var() functions and all output to the browser is escaped via php's htmlentities().  All database access is via PDO prepared statements.

Completing the admin side of the application would also be a next step.  Giving the pizza parlour owner the ability to change the state of exising orders, and to see which orders are still in the queue would be quite useful.

Another iteration of the app could also make a customer unique based on a certain identifier (email perhaps) so that you could track customer orders through time, and offer promotions to repeat customers, or pre-fill their preferred pizza at order time.  

Moving the toppings out into their own table with a linking table to pizza could be nice as well as adding some database integrity constraints between the customer and pizza table (foreign key on cusomter_id, for example).  This could have been done this iteration, but I found that I ran out of time.

Known Bugs
==========
There is one bug I came accross and was able to duplicate in a unit test.  For some reason, when the pizza + customer is saved to the db -- you loose the ability to ask the objects what data they were holding (and that's only some of the dat - some of it is just fine).  The work around is to reload the object from the db after you save it, but this shouldn't be necessary which is odd.


Deploying the Application
=========================
There is a live version of the application currently running on: http://sadpizza.com

If you would like to deploy a version locally you will need: 

php5.4 with the PDO msyql driver
mysql5.5 with INNODB support

Then, setting up the database should be as simple as executing the sql/initdb.sql file, and creating a vhost rooted at: www.sadpizza.com/htdocs/

The directory layout is: 

/README.md - this file
/NOTES - the original task as recieved
/phpunit.xml - configuration for phpunit
/htdocs - the web accessible part of the app.  
/sql - the database initialization script
/src - the classes for the application
/tests - unit tests for the application
/vendor - 3rd party software (bootstrap)
