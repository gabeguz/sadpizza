<?php
// Simple controller for handling form submission + redirecting to next step.
if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    header('Location: index.html');
}

require_once("../src/autoload.php");

$orderFromWeb = new PizzaOrderFromWeb($_REQUEST);
$order = $orderFromWeb->getOrder();
$pizza = $order->getPizza();
$customer = $order->getCustomer();
require_once("confirm.html");
