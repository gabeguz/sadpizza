CREATE DATABASE `sadpizza` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE sadpizza;
CREATE TABLE `customer` (
     `id` int(11) NOT NULL,
     `name` varchar(255) NOT NULL,
     PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `pizza` (
 `id` int(11) NOT NULL,
 `topping_1` varchar(100) DEFAULT NULL,
 `topping_2` varchar(100) DEFAULT NULL,
 `topping_3` varchar(100) DEFAULT NULL,
 `status` enum('todo','inprogress','done','') NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE USER `sadpizza`@`127.0.0.1` IDENTIFIED BY 'SzvaqgujIT5zKea6xzYqsx0bQa31ikNfHv0WvOK';
CREATE USER `sadpizza`@`localhost` IDENTIFIED BY 'SzvaqgujIT5zKea6xzYqsx0bQa31ikNfHv0WvOK';

GRANT ALL ON sadpizza.* TO `sadpizza`@`127.0.0.1`;
GRANT ALL ON sadpizza.* TO `sadpizza`@`localhost`;

FLUSH PRIVILEGES;

ALTER TABLE `pizza` ADD COLUMN `customer_id` int(11) NOT NULL AFTER id;
ALTER TABLE `customer` CHANGE COLUMN id id int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pizza` CHANGE COLUMN id id int(11) NOT NULL AUTO_INCREMENT;
